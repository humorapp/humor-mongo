@extends('layouts.ubold')

@section('css')
	<link href="/assets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
	<!-- Plugins css -->
    <link href="/assets/libs/mohithg-switchery/switchery.min.css" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
        
        <link href="/assets/libs/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
	<!-- Filter -->

    <!-- end row-->

    <div class="row">
    	<table id="basic-datatable" class="table dt-responsive nowrap w-100">
            <thead>
                <tr>
                    <th>Quiz</th>
                    <th>Score | {{ memeScore($meme->_id) }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $result)
                    <tr>
                        <td>{{ $result->quiz_id }}</td>
                        <td>{{ $result->score }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- end row -->
@endsection

@section('js')
	<script src="/assets/libs/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="/assets/js/pages/gallery.init.js"></script>
@endsection