@extends('layouts.quiz-template')

@section('css')
    <!-- Plugins css -->
        <link href="/assets/libs/toggle-switch/switch--radio-buttons.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
        <style>
            .input_hidden {
                position: absolute;
                left: -9999px;
            }

            .selected {
                background-color: rgba(0,255,0, 0.7);
            }

            .buttons  label {
                display: inline-block;
                cursor: pointer;
                text-align: center;
            }
            .buttons label:hover {
                background-color: rgba(0,255,0, 0.2);
            }

            .buttons  label img {
                padding: 3px;
                
            }
            .content-page{
                margin-left:0 !important;
            }
            @media only screen and (max-width: 500px) {
                .col-sm-4{
                    width: 32% !important;
                }
                .content-page{
                    padding-top:0 !important;
                    margin-top:0 !important;
                }
                .page-title img{
                    max-width: 100px !important;
                }
                .col-xs-4 img{
                    max-width: 40px;
                }
                .quiz-image{
                    width: 100%;
                    min-height: auto !important;
                }
            }
            li.disabled a{
                display: none;
            }
        </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('quiz-submit') }}" id="quizForm" class="form-horizontal">
                        @CSRF
                        <div id="progressbarwizard">
                            <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3" style="display:none;">
                                <li class="nav-item">
                                    <a href="#tab-intro" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" style="background:none !important;"></a>
                                </li>
                                @for($i = 0; $i < sizeof($memes); $i++)
                                    <li class="nav-item">
                                        <a href="#tab-{{ $i }}" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" style="background:none !important;"></a>
                                    </li>
                                @endfor
                                <li class="nav-item">
                                    <a href="#tab-30" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" style="background:none !important;"></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab-31" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" style="background:none !important;"></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#finish" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" style="background:none !important;"></a>
                                </li>
                            </ul>
                            <div class="tab-content b-0 mb-0 pt-0">
                                <div id="bar" class="progress mb-3" style="height: 7px;">
                                    <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success"></div>
                                </div>
                                <div class="tab-pane" id="tab-intro">
                                    <div class="row">
                                        <div class="col-12 text-center" style="min-height: 250px;">
                                            <h2>Humor Quiz!</h2>
                                            <p style="font-size:20px;">We show you some of our favorite memes and comedians and you tell us if they are funny, meh, or just lame/uncool.</p>
                                            <p style="font-size:20px;">If you prefer not to respond to a meme or don't know the comedian, you can skip by just pressing the "next" button!</p>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>
                                @for($i = 0; $i < sizeof($memes); $i++)
                                    <div class="tab-pane" id="tab-{{ $i }}">
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="min-height: 252px; margin-bottom:35px;">
                                                
                                                    <h3 class="text-center" style="font-size:16px; margin-top:0;">@if(!empty($memes[$i]->prompt)){{ $memes[$i]->prompt }} @else &nbsp; @endif</h3>
                                                
                                                <img data-url="{{ env('HUMOR_IMG_URL') . $memes[$i]->file }}" style="min-height: 250px; max-height: 265px; margin: 0 auto;" class="quiz-image">
                                            </div> <!-- end col -->
                                            <div class="buttons col-md-12" style="display: contents; margin-bottom: 35px;">
                                                
                                                    <input type="radio" name="item_{{ $memes[$i]->_id }}" id="{{ $memes[$i]->_id }}1" value="-1" /><label for="{{ $memes[$i]->_id }}1" class="col-md-4 col-sm-4 col-xs-4"><img src="/images/lame.png" /><br>Lame!</label>
                                                
                                                    <input type="radio" name="item_{{ $memes[$i]->_id }}" id="{{ $memes[$i]->_id }}2" value="1" /><label for="{{ $memes[$i]->_id }}2" class="col-md-4 col-sm-4 col-xs-4"><img src="/images/neutral.png" /><br> Okay!</label>
                                                
                                                    <input type="radio" name="item_{{ $memes[$i]->_id }}" id="{{ $memes[$i]->_id }}3" value="2" /><label for="{{ $memes[$i]->_id }}3" class="col-md-4 col-sm-4 col-xs-4"><img src="/images/laughed.png" alt="Super User" /><br> Funny!</label>
                                                    <br>
                                                    <div class="col-md-12 text-center">
                                                        <button type="button" class="reset btn btn-white">Reset</button>
                                                    </div>
                                            </div>
                                        </div> <!-- end row -->
                                    </div>
                                @endfor
                                <div class="tab-pane" id="tab-30">
                                    <div class="row">
                                        <div class="col-12 text-center" style="min-height: 250px;">
                                            <h2>You're Done! <i class=""></i></h2>
                                            <p style="font-size:19px;">We're calculating your results and comparing them against thousands of quiz submissions! In the meantime, we made this quiz because our research found <strong>100 percent</strong> of all couples that have been dating for at least one year share a sense of humor.</p>
                                            
                                            <p class="text-center">Click Next to Continue</p>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>
                                <div class="tab-pane" id="tab-31">
                                    <div class="row">
                                        <div class="col-12 text-center" style="min-height: 250px;">
                                            <h2>Almost There!</h2>
                                            <p style="font-size:19px;">We're launching an app aimed at finding potential partners based on your sense of humor and dating preferences.</p>
                                            <img src="/images/hero-bg-3b.jpg" class="text-center" style="max-width: 300px;">
                                            <br><br>
                                            <p style="font-size:19px;">The next {{ env('SPOTS_REMAINING') }} signups receive premium for life!</p>
                                            <input type="text" name="email" class="form-control" placeholder="Your Email">
                                            <br>
                                            <p class="text-center">Email Not Required for Results. <br> Click Next to Continue</p>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>
                                <div class="tab-pane" id="finish">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="text-center">
                                                <h2 class="mt-0">Results are Ready!</h2>
                                                <h3 class="mt-0">Click the button below to get your personality results!</h3>
                                                <br>
                                                <button type="submit" class="btn btn-success" id="submitQuiz">Get My Results</button>
                                            </div>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>
                                <ul class="list-inline mb-0 wizard" style="margin-top:35px;">
                                    <li class="previous list-inline-item">
                                        <a href="javascript: void(0);" class="btn btn-secondary">Previous</a>
                                    </li>
                                    <li class="next list-inline-item float-right">
                                        <a href="javascript: void(0);" class="btn btn-secondary">Next</a>
                                    </li>
                                </ul>
                            </div> <!-- tab-content -->
                        </div> <!-- end #progressbarwizard-->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div></div>
    <!-- end row -->
@endsection

@section('js')
    <!-- Plugins js-->
        <script src="/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <!-- Init js-->
        <script src="/assets/js/pages/form-wizard.init.js"></script>
        <script src="/assets/libs/jquery-mockjax/jquery.mockjax.min.js"></script>
        <script type="text/javascript">
            $('.buttons input:radio').each(function(){
                $(this).addClass('input_hidden');
            });

            $('.buttons label').on("click", function() {
                $(this).addClass('selected');
                $(this).siblings().removeClass('selected');
            });

            $('.buttons .reset').on("click", function() {
                var buttons = $(this).parent().parent().find("label");
                buttons.each(function(){
                    $(this).removeClass('selected');
                });
            });

            $("#submitQuiz").on("click", function(){
                $("#quizForm").submit();
            });

            var image;

            $(".quiz-image").each(function(){
                
                image = $(this).attr("data-url");
                $(this).delay(500).attr("src", image); 
            });
        </script>
@endsection