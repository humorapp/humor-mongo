<p class="text-left">Because <strong>100 percent (not kidding)</strong> of all couples that we've interviewed share a sense of humor. We're creating a free app that:</p>
<ul class="text-left">
	<li>Lets you see and message people who like you (without the blurry pics)</li>
	<li>Finds the best profiles based on your sense of humor and dating preferences</li>
	<li>Lets you share memes, disappearing selfies, and play mini-games with your matches</li>
	<li>Offers food and entertainment discounts for dates</li>
</ul>

<p class="text-left">Drop your email below to receive an exclusive invite when we launch! The next {{ env('SPOTS_REMAINING') }} signups receive premium for life!</p>