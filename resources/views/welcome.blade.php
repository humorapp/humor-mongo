<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="humor finds potential dates for you based on your sense of humor because all the best relationships start with a good laugh.">
    <meta name="author" content="humor">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="humor"/> <!-- website name -->
    <meta property="og:site" content="http://humorapp.co"/> <!-- website link -->
    <meta property="og:title" content="humor: laughter is in the air"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Potential dates based on your sense of humor because all the best relationships start with a good laugh."/> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="http://humorapp.co/images/site-image.png"/> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="http://humorapp.co"/> <!-- where do you want your post to link to -->
    <meta property="og:type" content="website"/>

    <!--title-->
    <title>humor: laughter is in the air</title>

    <!--favicon icon-->
    <link rel="icon" href="images/logo.png" type="image/png">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="stylesheets/magnific-popup.css">
    <!--Themify icon css-->
    <link rel="stylesheet" href="stylesheets/themify-icons.css">
    <!--animated css-->
    <link rel="stylesheet" href="stylesheets/animate.min.css">
    <!--ytplayer css-->
    <link rel="stylesheet" href="stylesheets/jquery.mb.YTPlayer.min.css">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="stylesheets/owl.carousel.min.css">
    <link rel="stylesheet" href="stylesheets/owl.theme.default.min.css">
    <!--custom css-->
    <link rel="stylesheet" href="stylesheets/style.css">
    <!--responsive css-->
    <link rel="stylesheet" href="stylesheets/responsive.css">
    <style>
    .counter-number {
  font-size: 32px;
  font-weight: 700;
  text-align: center;
}
.coming-box{
  float: left;
  width: 18%;
  text-align: center;
}

.counter-number span {
  font-size: 15px;
  font-weight: 400;
  display: block;
  text-align: center;
}
        th tr td{
            border: none;
        }
        th.highlight{
            color:white;
            background: #7200bb;
        }
        td.highlight{
            background: whitesmoke;
        }
        .table thead th,.table td, .table th{
            border:none;
        }
        #top-container{
            transition: 2s;
        }
        #PopupSignupForm_0{
        
        }
       @media only screen and (max-width: 600px) {
        .hero-content-left{
            margin-top:70px !important;
        }
        .navbar-toggler{
        display:none !important;
        }
        }
    </style>
</head>
<body>


<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->

<header class="header">
    <nav class="navbar navbar-expand-lg fixed-top bg-transparent">
        <div class="container">
            <a class="navbar-brand" href="index.html">
                <img src="images/logo-white.png" width="160" alt="logo" class="img-fluid"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button>
            <div class="collapse navbar-collapse h-auto" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto menu">
                    <li><a href="#contact" class="page-scroll">Home</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
    
<!--header section end-->


<!--body content wrap start-->
<div class="main">

    <!--hero section start-->
    <section class="hero-equal-height ptb-100 gradient-overlay"
             style="background: url('images/hero-bg-1.jpg')no-repeat center center / cover" id="top-container">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <!-- class="row justify-content-between" -->

                <div class="col-md-7 col-lg-6">
                    <div class="hero-content-left position-relative z-index text-white my-lg-0 my-md-5 my-sm-5 my-5">
                        <h1 class="text-white">humor</h1>
                        <p class="lead">We curate matches based on your sense of humor because all the best relationships start with a good laugh. <br> Launching in...</p>
                        <p><div data-countdown="2020/05/26" class="counter-number"></div></p>
                       <div class="action-btns download-btn mt-4">
                           
                           <a href="#signup" type="button" class="page-scroll btn solid-white-btn animated-btn mr-lg-3" style="margin-top:20px;">Join Early Access</a>
                       </div>

                    </div>
                </div>
                <div class="col-md-5 col-lg-6">
                    <div class="hero-img-right text-center position-relative z-index">
                        <img src="images/screenshot-match.png" alt="app" class="img-fluid" style="max-width: 350px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="shape-bottom">
            <img src="images/hero-shape-bottom.svg" alt="shape" class="bottom-shape img-fluid">
        </div>
    </section>
    <!--hero section end-->
    <section class="promo-new ptb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-lg-8">
                    <div class="section-heading text-center mb-2">
                        <h2>What Sets Us Apart</h2>
                        <p class="lead"><br></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <div class="new-single-promo rounded p-5 border-color-2 mt-4 text-center">
                        <div class="promo-img-wrap mb-4">
                            <img src="/images/match-making.jpg" width="160" alt="promo" class="img-fluid">
                        </div>
                        <div class="promo-content-wrap">
                            <h5>Stop Wasting Time</h5>
                            <p>On guessing how compatible you are with people. We do all the hard work to find you the best profiles.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="new-single-promo rounded p-5 border-color-3 mt-4 text-center">
                        <div class="promo-img-wrap mb-4">
                            <img src="/images/content-share.jpg" width="160" alt="promo" class="img-fluid">
                        </div>
                        <div class="promo-content-wrap">
                            <h5>Have Exciting Chats</h5>
                            <p>Send memes and play games to make your conversations more fun!<br><strong>Limited to verified profiles only.</strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="new-single-promo rounded p-5 border-color-1 mt-4 text-center">
                        <div class="promo-img-wrap mb-4">
                            <img src="/images/fun-discounts.jpg" width="160" alt="promo" class="img-fluid">
                        </div>
                        <div class="promo-content-wrap">
                            <h5>Share Quality Time</h5>
                            <p>Food and entertainment discounts for dates with your matches! <br><strong>Limited to verified profiles only.</strong></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    
                </div>
            </div>
            <div class="row justify-content-center" style="padding-top:50px;">
                <div class="col-md-6 col-lg-8">
                    <div class="section-heading text-center mb-2">
                        <h2>Take our Meme Quiz</h2>
                        <p class="lead">Our unique quiz reveals what your sense of humor says about your personality and how you compare with thousands of others.<br></p>
                        <img src="/images/quiz-banner.jpg" style="width: 100%;">
                        <br><br>
                        <a href="/quiz" class="btn btn-success solid-btn">Take the Quiz</a>
                    </div>
                </div>
            </div>
        </div>

    </section>

        <!--pricing section start-->
    <section id="pricing" class="pricing ptb-100" style="padding-top: 0; display:none;">
        <div class="container">
            <div class="row align-items-center justify-content-around">
                <div class="col-md-12 text-center" style="padding-bottom: 50px;">
                    <h3>The Best Features, All Free</h3>
                </div>
                <div class="col-md-12">
                    <div class="col-lg-12">
                        <div class="card popular-price text-center single-pricing-pack">
                            <div class="card-header py-4 border-0 pricing-header">
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="highlight">humor</th>
                                        <th>okCupid</th>
                                        <th>hinge</th>
                                        <th>tinder</th>
                                        <th>Bumble</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Unlimited Messaging</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Unlimited Daily Swipes</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Intelligent Matchmaking</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Unlimited Filters</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td><span class="ti-check"></span></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>See All Your Likes</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Match Activities</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>In-App Video Calling</td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Disappearing Photos <img src="https://i.pinimg.com/originals/91/60/c1/9160c163bd30d5568e0dfcdeeb5571eb.png" style="width: 15px;"></td>
                                        <td class="highlight"><span class="ti-check"></span></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--pricing section end-->

    <!--client section start-->
    <section  class="client-section ptb-100 gray-light-bg" id="signup">
        <div class="container">
            <!--clients logo start-->
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section-heading text-center mb-5">
                        <h2>Early Access</h2>
                        <p class="lead">
                            The first 2,000 sign-ups receive lifetime premium features. <strong>{{ number_format(env('SPOTS_REMAINING'),0) }} spots remain</strong>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-12">
                    @include('ads.mailchimp')
                </div>
            </div>
            <!--clients logo end-->
        </div>
    </section>
    <!--client section start-->
</div>
<!--body content wrap end-->

<!--footer section start-->
<footer class="footer-section" style="display:none;">
    <!--footer top start-->
    <div class="footer-top gradient-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12">
                    <div class="footer-nav-wrap text-white mb-0 mb-md-4 mb-lg-0">
                        <a class="d-block" href="#"><img src="images/logo-white.png" alt="footer logo" width="150" class="img-fluid mb-1"></a>
                        <p>Intrinsicly matrix high standards in niches whereas intermandated niche markets. Objectively harness competitive resources.</p>
                        <ul class="list-unstyled social-list mb-0">
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-facebook white-bg color-2 shadow rounded-circle"></span></a></li>
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-twitter white-bg color-2 shadow rounded-circle"></span></a></li>
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-linkedin white-bg color-2 shadow rounded-circle"></span></a></li>
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-dribbble white-bg color-2 shadow rounded-circle"></span></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-12">
                    <div class="footer-nav-wrap text-white">
                        <h5 class="text-white">Usefull Links</h5>
                        <ul class="list-unstyled footer-nav-list mt-3">
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Privacy Protection</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Safe Payments</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Terms of Services</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Documentation</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-12">
                    <div class="footer-nav-wrap text-white">
                        <h5 class="text-white">Company</h5>
                        <ul class="list-unstyled footer-nav-list mt-3">
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> About us Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Pricing Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Features Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> FAQ Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Contact Section</a></li>
                        </ul>
                    </div>
                </div>



                <div class="col-lg-4 col-md-4 col-12">
                    <div class="footer-nav-wrap text-white">
                        <h5 class="text-light footer-head">Newsletter</h5>
                        <p>Subscribe our newsletter to get our update. We don't send span email to you.</p>
                        <form action="#" class="newsletter-form mt-3">
                            <div class="input-group">
                                <input type="email" class="form-control" id="email" placeholder="Enter your email" required="">
                                <div class="input-group-append">
                                    <button class="btn solid-btn subscribe-btn btn-hover" type="submit">
                                        Subscribe
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--footer top end-->

    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg py-3">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-6 col-lg-5"><p class="copyright-text pb-0 mb-0">Copyrights © 2020.</p>
                </div>
            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>

<!--footer section end-->

<!--bottom to top button start-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="ti-angle-up"></span>
</button>
<!--bottom to top button end-->


<!--jQuery-->
<script src="scripts/jquery-3.4.1.min.js"></script>
<!--Popper js-->
<script src="scripts/popper.min.js"></script>
<!--Bootstrap js-->
<script src="scripts/bootstrap.min.js"></script>
<!--Magnific popup js-->
<script src="scripts/jquery.magnific-popup.min.js"></script>
<!--jquery easing js-->
<script src="scripts/jquery.easing.min.js"></script>
<!--wow js-->
<script src="scripts/wow.min.js"></script>
<!--owl carousel js-->
<script src="scripts/owl.carousel.min.js"></script>
<!--countdown js-->
<script src="scripts/jquery.countdown.min.js"></script>
<!--custom js-->
<script src="scripts/scripts.js"></script>
<script src="/assets/libs/countdown/jquery-countdown.min.js"></script>

        <!-- Countdown js -->
        <script src="/assets/libs/countdown/coming-soon.init.js"></script>

<script>
    
setInterval(function() {
            swapImage(); // code to be repeated
}, 5000); // every 1000 ms
      
      
function swapImage(){   
    var num = randomIntFromInterval(1, 4);
    img = "/images/hero-bg-" + num + ".jpg";
    $('#top-container').fadeTo('slow',0.95, function(){
        $(this).css('background-image', 'url(' + img + ')');
    }).delay(5000).fadeTo('slow');
}

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
}

$("#download").click(function(){
    $("#PopupSignupForm_0").toggle();
});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163662190-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163662190-1');
</script>
</body>
</html>