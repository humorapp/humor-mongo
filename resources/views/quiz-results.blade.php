@extends('layouts.quiz-template')

@section('css')
    <!-- Plugins css -->
        <link href="/assets/libs/toggle-switch/switch--radio-buttons.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
        <style>
            .input_hidden {
                    position: absolute;
                    left: -9999px;
                }

                .selected {
                    background-color: rgba(0,255,0, 0.7);
                }

                .buttons  label {
                    display: inline-block;
                    cursor: pointer;
                    text-align: center;
                }

                .buttons label:hover {
                    background-color: rgba(0,255,0, 0.2);
                }

                .buttons  label img {
                    padding: 3px;
                    
                }
                .content-page{
                    margin-left:0 !important;
                }
                p{
                    font-size:14.5px;
                    color:black;
                }
                @media only screen and (max-width: 500px) {
                    .content-page{
                        padding-top:0 !important;
                        margin-top:0 !important;
                    }
                    .page-title img{
                        max-width: 100px !important;
                    }
                }
        </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body tex">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="/images/{{ $top }}-humor.png" width="75"> <span style="font-size:48px;">&nbsp; + &nbsp;</span> <img src="/images/{{ $second }}-humor.png" width="75">
                            <h3>Your Humor is a Mix of {{ ucfirst($top) }} and {{ ucfirst($second) }}</h3>

                            @switch($top)
                                @case('dark')
                                    <p class="text-left"><strong>{{ ucfirst($top) }}</strong> was your favorite humor category, which means you're generally a very intelligent person, but you're often perceived as odd and deeply layered. {{ ucfirst($top) }} humor enthusiasts generally use this type of humor as a type of coping mechanism for their sometimes complex emotional innerworkings. <strong>Only {{ $stats["top"] }} percent</strong> of people have this as their top result.</p>
                                    @break

                                @case('witty')
                                    <p class="text-left"><strong>{{ ucfirst($top) }}</strong> was your favorite humor category, which means you love jokes that either challenge you or challenge others. {{ ucfirst($top) }} humor enthusiasts are generally very intelligent and enjoy learning. You also may find pleasure in teasing, which may be rooted in some insecurities. <strong>Only {{ $stats["top"] }} percent</strong> of people have this as their top result.</p>
                                    @break

                                @case('weird')
                                    <p class="text-left"><strong>{{ ucfirst($top) }}</strong> was your favorite humor category, which means you love weirdness in all shapes and sizes. The crazier the better, especially if the weirdness challenges what is considered socially appropriate or accepted. You're a little rebel inside! <strong>Only {{ $stats["top"] }} percent</strong> of people have this as their top result.</p>
                                    @break

                                @case('physical')
                                    <p class="text-left"><strong>{{ ucfirst($top) }}</strong> was your favorite humor category, which means you're either a little sadistic and find relief knowing someone is doing worse than you, or you find clumsiness (including your own) simply hilarious. <strong>Over {{ $stats["top"] }} percent</strong> of people have this as their top result.</p>
                                    @break

                                @case('contextual')
                                    <p class="text-left"><strong>{{ ucfirst($top) }}</strong> was your favorite humor category, which means you're politically aware and enjoy laughing at the current state of affairs. {{ ucfirst($top) }} humor enthusiasts are more likely to laugh at jokes that either make them feel attacked or make fun of those in their social circle(s). <strong>Over {{ $stats["top"] }} percent</strong> of people have this as their top result.</p>
                                    @break

                                @case('sexual')
                                    <p class="text-left"><strong>{{ ucfirst($top) }}</strong> was your favorite humor category, which means you've got a dirty mind, but that's not a bad thing! It means you or someone you know closely has had some embarassingly relateable intimate moments. You may also use this type of humor to relieve sexual frustration. <strong>Over {{ $stats["top"] }} percent</strong> of people have this as their top result.</p>
                                    @break

                                @default
                                    @break
                            @endswitch

                            @switch($second)
                                @case('dark')
                                    <p class="text-left"><strong>{{ ucfirst($second) }}</strong> was your second favorite humor category, which means you're generally a very intelligent person, but you're often perceived as odd and deeply layered. {{ ucfirst($second) }} humor enthusiasts generally use this type of humor as a type of coping mechanism for their sometimes complex, emotional innerworkings. <strong>Only {{ $stats["second"] }} percent</strong> of people have this as their second result.</p>
                                    @break

                                @case('witty')
                                    <p class="text-left"><strong>{{ ucfirst($second) }}</strong> was your second favorite humor category, which means you love jokes that either challenge you or challenge others. {{ ucfirst($second) }} humor enthusiasts are generally very intelligent and enjoy learning. You also may find pleasure in teasing, which may be rooted in some insecurities. <strong>Only {{ $stats["second"] }} percent</strong> of people have this as their second result.</p>
                                    @break

                                @case('weird')
                                    <p class="text-left"><strong>{{ ucfirst($second) }}</strong> was your second favorite humor category, which means you love weirdness in all shapes and sizes. The crazier the better, especially if the weirdness challenges what is considered socially appropriate or accepted. You're a little rebel inside! <strong>Only {{ $stats["second"] }} percent</strong> of people have this as their second result.</p>
                                    @break

                                @case('physical')
                                    <p class="text-left"><strong>{{ ucfirst($second) }}</strong> was your second favorite humor category, which means you're either a little sadistic and find relief knowing someone is doing worse than you, or you find clumsiness (including your own) simply hilarious. <strong>Over {{ $stats["second"] }} percent</strong> of people have this as their second result.</p>
                                    @break

                                @case('contextual')
                                        <p class="text-left"><strong>{{ ucfirst($second) }}</strong> was your second favorite humor category, which means you're politically aware and enjoy laughing at the current state of affairs. {{ ucfirst($second) }} humor enthusiasts are more likely to laugh at jokes that either make them feel attacked or make fun of those in their social circle(s). <strong>Over {{ $stats["second"] }} percent</strong> of people have this as their second result.</p>
                                    @break

                                @case('sexual')
                                    <p class="text-left"><strong>{{ ucfirst($second) }}</strong> was your second favorite humor category, which means you've got a dirty mind, but that's not a bad thing! It means you or someone you know closely has had some embarassingly relateable intimate moments. You may also use this type of humor to relieve sexual frustration. <strong>Over {{ $stats["second"] }} percent</strong> of people have this as their second result.</p>
                                    @break
                                @default
                                    @break
                            @endswitch
                        </div>
                        <div class="col-md-12 text-center">
                            <p><strong>Only {{ $stats["both"] }} percent of people have your exact sense of humor!</strong></p>
                            <p>Share Your Results: <em>https://humorapp.co/quiz-results/{{$top}}/{{$second}}</em></p>
                            <a href="/quiz" class="btn btn-danger" style="padding: 2px 10px; margin-right: 30px; margin-top: -7px;">Restart Quiz</a>
                            <div class="fb-share-button" 
                            data-href="{{ $url }}"
                            data-quote="I took a quiz and found out my sense of humor is a combination of {{ ucfirst($top) }} and {{ ucfirst($second) }}."
                            data-size="large"
                            data-layout="button">
                          </div>        
                            <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
                                fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));</script>
                        </div>
                    </div>
                    @if(empty($email))
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <br>
                            <span style="font-size:8px;">Epstein didn't kill himself</span>
                            <hr></hr>
                            
                            <h3>Why did we make this quiz?</h3>
                            @include('ads.text2')
                            @include('ads.mailchimp')

                        </div>
                    </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <br>
                                <span style="font-size:8px;">Epstein didn't kill himself</span>
                                <hr></hr>
                                
                                <h3>You're on the VIP list! See you soon.</h3>

                            </div>
                        </div>
                    @endif
                    <div class="row" style="display: none;">
                        <div class="col-md-12">
                            <hr></hr>

                            <p>We're launching a new app to find potential dates based on your sense of humor. See all your likes, exchange memes, and play games with them for free.</p>
                            <img src="https://humorapp.co/images/fb-ad-2.jpg" style="width: 100%;">
                            <br><br>
                            <p>
                            First 2,000 sign-ups get premium access for life. Secure your spot by dropping your email below. <strong>{{ number_format(env('SPOTS_REMAINING'),0) }} spots remain</strong></p>
                        </div>
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div></div>
    <!-- end row -->
@endsection

@section('js')
    <!-- Plugins js-->
        <script src="/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

        <!-- Init js-->
        <script src="/assets/js/pages/form-wizard.init.js"></script>
        <script src="/assets/libs/toggle-switch/switch-toggle-buttons.js"></script>
        <script src="/assets/libs/multiselect/js/jquery.multi-select.js"></script>
        <script src="/assets/libs/select2/js/select2.min.js"></script>
        <script src="/assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
        <script src="/assets/libs/bootstrap-select/js/bootstrap-select.min.js"></script>
        <script src="/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        <script src="/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
        <script src="/assets/js/pages/form-advanced.init.js"></script>

        <script type="text/javascript">
            $('.buttons input:radio').each(function(){
                $(this).addClass('input_hidden');
            });

            $('.buttons label').on("click", function() {
                $(this).addClass('selected');
                $(this).siblings().removeClass('selected');
            });

            $('.buttons .reset').on("click", function() {
                var buttons = $(this).parent().parent().find("label");
                console.log($(this).html());
                buttons.each(function(){
                    $(this).removeClass('selected');
                });
            });

            $("#submitQuiz").on("click", function(){
                $("#quizForm").submit();
            });

            jQuery(document).ready(function ($) { 
                $('body').on("click", "#mc-embedded-subscribe", function() {
                    $.ajax({
  url: "https://humorapp.co/webhooks/{{ $quizID }}-{{ $top }}-{{ $second}}",
  context: document.body
}).done(function() {
  //$( this ).addClass( "done" );
});
                });
            });
        </script>
@endsection


