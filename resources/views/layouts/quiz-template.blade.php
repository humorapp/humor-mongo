<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ $title }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="" name="description" />
        <meta content="humor" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <meta name="description"
          content="{{ $description }}">
        <link rel="shortcut icon" href="/images/logo.png">
        <meta property="og:site_name" content="humor"/> <!-- website name -->
    <meta property="og:site" content="https://humorapp.co"/> <!-- website link -->
    <meta property="og:title" content="{{ $title }}"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="{{ $description }}"/> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="{{ $image }}"/> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="{{ $url }}"/> <!-- where do you want your post to link to -->
    <meta property="og:type" content="website"/>
    <meta property="fb:app_id" content="1338106493064038"/>

        <!-- Plugins css -->
        <link href="/assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="/assets/css/bootstrap-saas.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="/assets/css/app-saas.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <link href="/assets/css/bootstrap-saas-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
        <link href="/assets/css/app-saas-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

        <!-- icons -->
        <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />

        @yield('css')
    </head>
    
    <body data-layout-mode="two-column" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "condensed", "showuser": false}, "topbar": {"color": "light"}, "showRightSidebarOnPageLoad": false}'>
        <div id="fb-root"></div>

        <!-- Begin page -->
        <div id="wrapper">


            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                    </div>
                                    <h4 class="page-title text-center"><a href="/" target="_blank"><img src="/images/logo-black.png" style="max-width: 200px;"></a></h4>
                                </div>
                            </div>
                        </div>
                        @yield('content')
                        
                    </div> <!-- container -->

                </div> <!-- content -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Vendor js -->
        <script src="/assets/js/vendor.min.js"></script>

        <!-- Plugins js-->
        <script src="/assets/libs/flatpickr/flatpickr.min.js"></script>


        <script src="/assets/libs/selectize/js/standalone/selectize.min.js"></script>


        <!-- App js-->
        <script src="/assets/js/app.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163662190-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163662190-1');
</script>
        @yield('js')
    </body>
</html>