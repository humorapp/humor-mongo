<div class="left-side-menu">
    <div class="h-100" >
        <div class="sidebar-content">
            <div class="sidebar-icon-menu h-100" data-simplebar>
                <!-- LOGO -->
                <a href="index.html" class="logo">
                    <span>
                        <img src="/images/logo.png" alt="" height="28">
                    </span>
                </a>
                <nav class="nav flex-column" id="two-col-sidenav-main">
                    <a class="nav-link" href="/" data-toggle="tooltip" data-placement="right"
                        title="Dashboard" data-trigger="hover">
                        <i data-feather="home"></i>
                    </a>
                    <a class="nav-link" href="/memes" data-toggle="tooltip" data-placement="right"
                        title="Memes" data-trigger="hover">
                        <i data-feather="image"></i>
                    </a>
                </nav>
            </div>
            <!--- Sidemenu -->
            @include('layouts.sidebar-main')
            <div class="clearfix"></div>
        </div>
        <!-- End Sidebar -->
    </div>
    <!-- Sidebar -left -->
</div>