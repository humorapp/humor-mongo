<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meme as Memes;
use App\Result as Results;
use Redirect;
use DB;
use Mail;

class QuizController extends Controller
{
    public function browseImages($t = NULL){
    	$data = [
    		"memes" => Memes::all(),
            "title" => "Humor Quiz! What Does Your Sense of Humor Say About You?",
            "description" => "Take our quiz to find out your humor type! Sign-up to find potential dates based on your sense of humor because all the best relationships start with a good laugh.",
            "image" => "https://humorapp.co/images/site-image.jpg"
    	];

    	return view('admin.memes', $data);
    }

    public function browseQuizzes(){

        $data = [
            "quizzes" => Results::distinct()->get(['quiz_id'])
        ];

        return view('admin.quizzes', $data);
    }

    public function readMeme($id){
        $data = [
            "meme" => Memes::where('_id', $id)->first(),
            "results" => Results::where('meme_id', $id)->get(),
            "title" => "Humor Quiz! What Does Your Sense of Humor Say About You?",
            "description" => "Take our quiz to find out your humor type! Sign-up to find potential dates based on your sense of humor because all the best relationships start with a good laugh.",
            "image" => "https://humorapp.co/images/site-image.jpg"
        ];

        return view('admin.meme', $data);
    }

    public function addQuiz(Request $request){
    	
    	$memes = Memes::all();

    	$sexual = $dark = $witty = $weird = $physical = $contextual = 0;


    	foreach($memes as $meme){
    		$var = "item_$meme->_id";

    		switch ($meme->type) {
    			case 'dark':
    				$dark += $request->{$var};
    				break;
    			case 'witty':
    				$witty += $request->{$var};
    				break;
    			case 'weird':
    				$weird += $request->{$var};
    				break;
    			case 'physical':
    				$physical += $request->{$var};
    				break;
    			case 'contextual':
    				$contextual += $request->{$var};
    				break;
    			case 'sexual':
    				$sexual += $request->{$var};
    				break;
    			default:
    				break;
    		}
    	}

    	$results = ["dark" => $dark, "witty" => $witty, "weird" => $weird, "physical" => $physical, "contextual" => $contextual, "sexual" => $sexual];
    	asort($results);

    	$keys = array_keys($results);
        $top = $keys[5];
        $second = $keys[4];

        $items = $request->input();
        $form = array_shift($items);
        $quiz_id = generateRandomNumString(8);

        foreach ($items as $key => $value) {
            if($key != "email"){
                DB::table('results')->insert([
                    'quiz_id' => $quiz_id,
                    'meme_id' => str_replace("item_", "", $key),
                    'score' => $value
                ]);
            }
        }

        if($results["dark"] == 0 && $results["witty"] == 0 && $results["sexual"] == 0 && $results["physical"] == 0 && $results["weird"] == 0 && $results["contextual"] == 0){
            echo "<div style='width:330px; margin:0 auto; padding-top:3%;'><h2>You skipped all the questions :( </h2> <br><br>Please restart the <a href='/quiz'>humor quiz</a></div>";
            return "";
        }

        $story = NULL;
    	
        $data = [
            "top" => $top,
            "second" => $second,
            "stats" => getStats($top, $second),
            "results" => $results,
            "quizID" => $quiz_id,
            "title" => "My Sense of Humor is " . ucfirst($top) . " and " . ucfirst($second),
            "description" => "Take this humor quiz to find out what your sense of humor says about you!",
            "image" => "https://humorapp.co/images/quiz-banner.png",
            "url" => "https://humorapp.co/quiz-results/$top/$second",
            "story" => $story,
            "email" => $request->email
        ];

        if($top == "sexual" || $top == "physical" || $top == "contextual"){
            $data["story"] = getCoupleComparison($top, $second);
        }

        if(!empty($request->email)){
            DB::table('surveys')->insert([
                'quiz_id' => $quiz_id,
                'email' => $request->email
            ]);

            Mail::raw($quiz_id . ' submitted their email: ' . $request->email, function($message)
            {
                $message->from('hello@mg.humorapp.co', 'Humor');
                $message->to('2404412060@vtext.com');
            });
        }

        return view('quiz-results', $data);
    }

    public function quizResults(Request $request, $top = NULL, $second = NULL){

        if(is_null($top) || is_null($second)){
            return Redirect::to('/quiz');
        }


        $fb = false;
        $server = "none";

        if(array_key_exists("HTTP_REFERER", $_SERVER)){
            $server = $_SERVER["HTTP_REFERER"];
        }


        if (strpos($server, 'facebook') !== false) {
            $fb = true;
        }

        if(isset($request->fbclid) || $fb){
            //return Redirect::to('/quiz');
        }

        // get stats

        $story = NULL;

        $data = [
            "top" => $top,
            "second" => $second,
            "stats" => getStats($top, $second),
            "results" => ["dark" => "", "witty" => "", "weird" => "", "physical" => "", "contextual" => "", "sexual" => ""],
            "quizID" => "guest",
            "title" => "My Sense of Humor is " . ucfirst($top) . " and " . ucfirst($second),
            "description" => "Take this humor quiz to find out what your sense of humor says about you!",
            "image" => "https://humorapp.co/images/quiz-banner.png",
            "url" => "https://humorapp.co/quiz-results/$top/$second",
            "story" => $story,
            "email" => NULL
        ];

        if($top == "sexual" || $top == "physical" || $top == "contextual"){
            $data["story"] = getCoupleComparison($top, $second);
        }

        return view('quiz-results', $data);
    }
}
