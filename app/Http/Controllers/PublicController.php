<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as Users;
use App\Meme as Memes;
use Carbon\Carbon as Carbon;
use DB;
use Redirect;

class PublicController extends Controller
{
    public function index(){
        $data = [
            "results" => ["dark" => "", "witty" => "", "weird" => "", "physical" => "", "contextual" => "", "sexual" => ""],
            "quizID" => ""
        ];

        getCoupleComparison("sexual", "physical");

        return view('welcome', $data);
    }

    public function browseUsers(){

        $data = [
            "user" => Users::all()
        ];
       
        dd($data);

    	return view('welcome', $data);
    }

    public function browseQuiz(){
        //$memes = Memes::all();

        // create category collections
        //$memes = collect();
        $memes = Memes::where('type', "dark")->where('active',1)->inRandomOrder()->limit(4)->get();
        $memes .= Memes::where('type', "witty")->where('active',1)->inRandomOrder()->limit(4)->get();
        $memes .= Memes::where('type', "sexual")->where('active',1)->inRandomOrder()->limit(4)->get();
        $memes .= Memes::where('type', "physical")->where('active',1)->inRandomOrder()->limit(4)->get();
        $memes .= Memes::where('type', "weird")->where('active',1)->inRandomOrder()->limit(4)->get();
        $memes .= Memes::where('type', "contextual")->where('active',1)->inRandomOrder()->limit(4)->get();
       

        $memes = json_decode(str_replace("}][{", "},{", $memes));
        

        $data = [
            "memes" => $memes,
            "title" => "What Does Your Sense of Humor Say About You?",
            "description" => "Take our quiz to find out your humor type! Sign-up to find potential dates based on your sense of humor because all the best relationships start with a good laugh.",
            "image" => "https://humorapp.co/images/site-image.jpg",
            "url" => "https://humorapp.co/quiz"
        ];

        return view('quiz', $data);
    }

    public function readUser($id){
    	$data = [
    		"user" => Users::where('_id', $id)->first()
    	];

    	return view('admin.user', $data);
    }

    public function detectFB(Request $request){
        $fb = false;
        $server = "none";

        if(array_key_exists("HTTP_REFERER", $_SERVER)){
            $server = $_SERVER["HTTP_REFERER"];
        }


        if (strpos($server, 'facebook') !== false) {
            $fb = true;
        }

        if(isset($request->fbclid) || $fb){
            //return Redirect::to('/quiz');
        }
    }
}
