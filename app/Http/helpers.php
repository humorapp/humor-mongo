<?php

use Carbon\Carbon as Carbon;
use App\Result as Results;
use App\Meme as Memes;
use App\Stat as Stats;
use App\Pairing as Pairings;

function formatGMTstring($date, $format){
    $date =  new DateTime($date);
    return $date->format($format);
}

function dateToISO($date){
    $date =  new DateTime($date);
    return $date->format("c");
}

function DateDifference($start, $end){
    $datetime1 = new DateTime($start);
    $datetime2 = new DateTime($end);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%a');
}


function convertHTMLTime($stamp)
{
    
    if(!is_null($stamp)){
        return date("m-d-Y", strtotime($stamp));
    }
    return "";
}

function calculateAge($stamp){
    //$today = strtotime(Date('m-d-Y'));
    //$date = Carbon::createFromFormat('Y-m-d H:i:s',$stamp);
    $years = Carbon::parse($stamp)->setTimezone('UTC');
    return $years;
}

function convertTimestamp($stamp)
{
    if(!is_null($stamp)){
        return date('M j Y g:i A', strtotime($stamp));
    }
    return "";
}

function convertHTMLTimetoUnix($date){
// takes $formated_datetime and converts to "UNIX timestamp":
$unix_timestamp = STRTOTIME($date);
 
// converts $unix_timestamp to "normal" formated_datetime:         
return DATE("Y-m-d H:i:s",$unix_timestamp); 
}

function convertHTMLTime2($date){
    $parts = explode('-',$date);
    $yyyy_mm_dd = $parts[2] . '-' . $parts[0] . '-' . $parts[1];
    return $yyyy_mm_dd;
}

function exposeArray($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function bsAlert($alert, $class){
    $alertID = rand(1111,9999);
    echo '<div class="alert alert-' . $class . ' alert-styled-left" id="alert1">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            ' . $alert . '
        </div>
        <script>setTimeout("alertHide(1)", 3000); </script>
        ';
}

function displayMatch($matcher, $matchee){
    $matcher = Connections::where('id', $matcher)->first()->toArray();
    $matchee = Connections::where('id', $matchee)->first()->toArray();
    return ["matcher" => $matcher, "matchee" => $matchee];
}

function hasMatchMailSent($matcher, $matchee){
    if(Matches::where('author', Auth::user()->id)->where('matchee', $matchee)->where('matcher', $matcher)->count() > 0 || Matches::where('author', Auth::user()->id)->where('matcher', $matchee)->where('matchee', $matcher)->count() > 0){
        return true;
    }
    return false;
}

function getOccupation($id){
    $occupation = Occupations::where('id', $id)->first();
    if(is_object($occupation)){
        return $occupation->title;
    }
    return "";
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomNumString($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function weighResultScore($score){
    // convert elasticsearch's scoring system to something more human friendly
    switch ($score) {
        case $score > env('ELASTIC_STRONG'):
            return "Strong";
            break;
        case $score > env('ELASTIC_WEAK') && $score <= env('ELASTIC_OKAY'):
            return "Okay";
            break;
        default:
            return "Weak";
            break;
    }
}

function stringClean($url){
    $url = str_replace(";", ",", $url);
    $url = str_replace("w/", "with", $url);
    $url = str_replace("/", " or ", $url);
    $url = str_replace("&", "%26", $url);

    return $url;
}

function arrayReplace ($find, $replace, $array) {
    if (!is_array($array)) {
        return str_replace($find, $replace, $array);
    }

    $newArray = [];
    foreach ($array as $key => $value) {
        $newArray[$key] = arrayReplace($find, $replace, $value);
    }
    return array_unique($newArray);
}

function memeScore($id){
    return Results::where('meme_id', $id)->sum('score');
}

function getQuizResult($id){
    
    $results = Results::where('quiz_id', $id)->get();

    $sexual = $dark = $witty = $weird = $physical = $contextual = 0;

    foreach($results as $result){

        $meme = Memes::where('_id', $result->meme_id)->first();

        switch ($meme->type) {
            case 'dark':
                $dark += $result->score;
                break;
            case 'witty':
                $witty += $result->score;
                break;
            case 'weird':
                $weird += $result->score;
                break;
            case 'physical':
                $physical += $result->score;
                break;
            case 'contextual':
                $contextual += $result->score;
                break;
            case 'sexual':
                $sexual += $result->score;
                break;
            default:
                break;
        }
    }

    $response = ["dark" => $dark, "witty" => $witty, "weird" => $weird, "physical" => $physical, "contextual" => $contextual, "sexual" => $sexual];
    
    asort($response);

    $keys = array_keys($response);

    DB::table('stats')->insert([
        'top' => $keys[5],
        'second' => $keys[4]
    ]);

    return $response;
}

function getStats($top, $second){
    $stats = Stats::count();
    $topCount = number_format((Stats::where('top', $top)->count() / $stats) * 100, 1);
    $secondCount = number_format((Stats::where('second', $second)->count() / $stats) * 100,1);
    $bothCount = number_format((Stats::where('top', $top)->where('second', $second)->count() / $stats) * 100,1);

    return ["top" => $topCount, "second" => $secondCount, "both" => $bothCount];
}

function getCoupleComparison($top, $second){
    // we want to present to the user what percentage of couples share their results. make them optimistic.

    // find all couples where the pairs are exactly the same for each partner
    

    // find all couples where the top result is found for each partner
    $exactFirstResults = Pairings::where('p1_top', $top)->where('p2_top', $top)->get();
    
    // $user_list = DB::table('users')
    $results = DB::table('pairings')->where(function($query) use ($top, $second){
        $query->where('p1_top', $top);
        $query->orWhere('p2_top', $top);
        $query->orWhere('p1_second', $top);
        $query->orWhere('p2_second', $top);
    })
    ->get();

   return [
            "matches" => round(($results->count() / Pairings::count() * 100), 0),
            "length" => round($results->avg('years'),1)
        ];

}