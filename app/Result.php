<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Result extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'results';
}
