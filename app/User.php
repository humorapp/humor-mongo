<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract
{
    use AuthenticableTrait;
    use CanResetPassword;

    protected $connection = 'mongodb';
    protected $collection = '_User';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id', 'name', 'email', 'username', '_hashed_password', '_created_at', '_updated_at', 'phoneNumber', 'age', 'gender', 'school',
        'familyPlansPreference','ethnicityPreference','drinkingPreference','drugPreference','educationLevel','humorCompatibility','jobTitle', 'city', 'lookingFor', '_wperm', '_rperm', '_acl'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '_hashed_password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAuthPassword()
    {
        return $this->_hashed_password;
    }
}
