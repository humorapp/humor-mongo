<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Survey extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'surveys';
}
