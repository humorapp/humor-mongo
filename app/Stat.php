<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Stat extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'stats';
}
