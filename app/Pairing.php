<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pairing extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'pairings';
}
