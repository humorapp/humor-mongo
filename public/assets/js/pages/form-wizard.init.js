$(document).ready(function() {
    "use strict";
    $("#progressbarwizard").bootstrapWizard({
        onTabShow: function(t, r, a) {
            var o = (a + 1) / r.find("li").length * 100;
            $("#progressbarwizard").find(".bar").css({
                width: o + "%"
            })
        },
        onInit: function (event, current) {
            $('.actions > ul > li:first-child').attr('style', 'display:none');
        },
        onStepChanged: function (event, current, next) {
            if (current > 0) {
                $('.actions > ul > li:first-child').attr('style', '');
            } else {
                $('.actions > ul > li:first-child').attr('style', 'display:none');
            }
        }
    });
});