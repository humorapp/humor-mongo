<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');
Route::get('/users', 'PublicController@browseUsers');
Route::get('/user/{id}', 'PublicController@readUser');

Route::get('/memes', 'QuizController@browseImages');
Route::get('/memes/{id}', 'QuizController@readMeme');

Route::get('/quiz', 'PublicController@browseQuiz');

Route::get('/quiz-results/{top?}/{second?}', 'QuizController@quizResults')->name('quiz-results');
Route::post('/quiz-results', 'QuizController@addQuiz')->name('quiz-submit');

Route::get('/quizzes', 'QuizController@browseQuizzes');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/fb', 'PublicController@detectFB');

Route::get('/webhooks/{source?}', 'WebhooksController@index');
Route::post('/webhooks/{source}', 'WebhooksController@index');

